/* GET home page. */
var WineService = require('../services/WineService');
var CountryService = require('../services/CountryService');
var PriceByVolumeService = require('../services/PriceByVolumeService');
var VolumeService = require('../services/VolumeService');
var WineMakerService = require('../services/WineMakerService');
var WineTypeService = require('../services/WineTypeService');

var Wine = require('../services/models/wine');

exports.index = function(req, res, next) {
	Wine.find({}, function(err, wines) {
		if (err) {
			return next(err);
		}
		res.render('index', {
			wines: wines
		});
	});
};

exports.loadCountriesFromXML = function(req, res, next) {
	CountryService.addCountriesFromXML('/../public/xml/country.xml', function(result) {
		res.send(200);
	});
};