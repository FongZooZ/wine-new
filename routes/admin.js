var WineType = require('../services/models/wineType');
var Volume = require('../services/models/volume');
var Country = require('../services/models/country');
var WineMaker = require('../services/models/wineMaker');
var Wine = require('../services/models/wine');

var AdminService = require('../services/AdminService');
var WineTypeService = require('../services/WineTypeService');
var VolumeService = require('../services/VolumeService');
var CountryService = require('../services/CountryService');
var WineMakerService = require('../services/WineMakerService');
var WineService = require('../services/WineService');

exports.login = function(req, res, next) {
	res.render('login');
};

exports.checkLogin = function(req, res, next) {
	var adminData = req.body;
	AdminService.checkLogin(adminData.username || "", adminData.password || "", function(err, canLogin) {
		if (err) {
			return next(err);
		}
		if (canLogin) {
			var user = canLogin;
			req.session.user = user;
			req.session.user.password = "trustme,thiscanneverbearealpassword,hahahah";
			res.redirect("/admin");
		} else {
			req.session.error = "Invalid username or password";
			res.redirect("/login");
		}
	});
};

//add
exports.addWineType = function(req, res, next) {
	var wineTypeData = req.body;
	WineTypeService.addWineType(wineTypeData, function(err, wineType) {
		res.send(200);
	});
}

exports.addVolume = function(req, res, next) {
	var volumeData = req.body;
	VolumeService.addVolume(volumeData, function(err, volume) {
		res.send(200);
	});
}

exports.addWineMaker = function(req, res, next) {
	var wineMakerData = req.body;
	WineMakerService.addWineMaker(wineMakerData, function(err, wineMaker) {
		res.send(200);
	});
};

exports.addWine = function(req, res, next) {
	var wineData = req.body;
	WineService.addWine(wineData, function(err, wine) {
		res.send(200);
	});
};

// get
exports.getCountries = function(req, res, next) {
	Country.find({}, function(err, countries) {
		res.send(countries);
	});
};

exports.getPriceByVolumes = function(req, res, next) {
	PriceByVolume.find({}, function(err, priceByVolumes) {
		res.send(priceByVolumes);
	});
};

exports.getVolumes = function(req, res, next) {
	Volume.find({}, function(err, volumes) {
		res.send(volumes);
	});
};

exports.getWines = function(req, res, next) {
	Wine.find({}, function(err, wines) {
		res.send(wines);
	});
};

exports.getWineMakers = function(req, res, next) {
	WineMaker.find({}, function(err, wineMakers) {
		res.send(wineMakers);
	});
};

exports.getWineTypes = function(req, res, next) {
	WineType.find({}, function(err, wineTypes) {
		res.send(wineTypes);
	});
};

exports.logout = function(req, res, next) {
	req.session.destroy();
	res.redirect('/');
};

exports.index = function(req, res, next) {
	Wine.find({}, function(err, wines) {
		if (err) {
			return next(err);
		}
		WineType.find({}, function(err, wineTypes) {
			if (err) {
				return next(err);
			}
			WineMaker.find({}, function(err, wineMakers) {
				if (err) {
					return next(err);
				}
				res.render('admin/wine', {
					wines: wines,
					wineTypes: wineTypes,
					wineMakers: wineMakers
				});
			});
		});
	});
};

exports.wineMaker = function(req, res, next) {
	WineMaker.find({}, function(err, wineMakers) {
		if (err) {
			return next(err);
		}
		Country.find({}, function(err, countries) {
			if (err) {
				return next(err);
			}
			res.render('admin/wineMaker', {
				wineMakers: wineMakers,
				countries: countries
			});
		});
	});
};

exports.country = function(req, res, next) {
	Country.find({}, function(err, countries) {
		if (err) {
			return next(err);
		}
		res.render('admin/country', {
			countries: countries
		});
	});

}

exports.volume = function(req, res, next) {
	Volume.find({}, function(err, volumes) {
		if (err) {
			return next(err);
		}
		res.render('admin/volume', {
			volumes: volumes
		})
	});
};

exports.wineType = function(req, res, next) {
	WineType.find({}, function(err, wineTypes) {
		if (err) {
			return next(err);
		}
		res.render('admin/wineType', {
			wineTypes: wineTypes
		});
	});

};

exports.getCountryById = function(req, res, next) {
	CountryService.getOneCountry(req.body.countryID, function(err, country) {
		if (err) {
			return next(err);
		}
		console.log(country);
		res.send(country);
	});
};