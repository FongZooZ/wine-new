var Admin = require('./models/admin');

var crypto = require('crypto');

var hash = function hash(string) {
	string = !string ? "" : string;
	var md5 = crypto.createHash('md5');
	return md5.update(string).digest('hex');
}

/**
 * check whether username and password is correct
 * @param  {string}   username
 * @param  {string}   password
 * @param  {Function} callback: function (err, result) {...} result is true if correct, vice versa
 * @return {void}
 */
var checkLogin = function checkLogin(username, password, callback) {
	Admin.findOne({
		username: new RegExp("^" + username.toLowerCase(), "i"),
		password: hash(password)
	}, function(err, admin) {
		if (err) {
			return callback(err);
		}
		if (admin) {
			callback(null, admin);
		} else {
			callback(null, false);
		}
	});
}

module.exports = {
	checkLogin: checkLogin
}