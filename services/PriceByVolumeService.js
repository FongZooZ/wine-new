var PriceByVolume = require('./models/priceByVolume');

var getPriceByVolume = function getPriceByVolume(wineMakerId, wineId, volumeId, callback) {

	PriceByVolume.findOne({
		wineMakerId: wineMakerId,
		wineId: wineId,
		volumeId: volumeId
	}, function(err, thisWineType) {
		if (err) {
			return callback(err);
		}
		if (thisWineType) {
			callback(null, thisWineType);
		} else {
			callback(new Error("Cannot get"));
		}
	});
}

var getPriceByVolumeByWineId = function getPriceByVolumeByWineId(wineId, callback) {
	PriceByVolume.find({
		wineId: wineId
	}, function(err, thisWineType) {
		if (err) {
			return callback(err);
		}
		if (thisWineType) {
			callback(null, thisWineType);
		} else {
			callback(new Error("Cannot get"));
		}
	});
}

var addPriceByVolume = function addPriceByVolume(data, callback) {
	PriceByVolume.findOne({
		wineMakerId: data.wineMakerId,
		wineId: data.wineId,
		volumeId: data.volumeId,
	}, function function_name(err, thisPriceByVolume) {
		if (err) {
			return callback(err);
		}
		if (!thisPriceByVolume) {
			PriceByVolume.create({
				wineMakerId: data.wineMakerId,
				wineId: data.wineId,
				volumeId: data.volumeId,
				price: Number
			}, function(err, priceByVolume) {
				if (err) {
					return callback(err);
				}
				if (priceByVolume) {
					callback(null, true);
				} else {
					callback(new Error("something error, can't create this price by volume"));
				}
			});
		} else {
			callback(new Error("Exist"));
		}
	});
}

var editPriceByVolume = function editPriceByVolume(wineMakerId, wineId, volumeId, data, callback) {

	PriceByVolume.findOneAndUpdate({
		wineMakerId: wineMakerId,
		wineId: wineId,
		volumeId: volumeId
	}, {
		price: data.price
	}, {
		new: false,
		upsert: false,
		multi: false
	}, function(err, thisPriceByVolume) {
		if (err) {
			return callback(err);
		}
		if (thisPriceByVolume) {
			callback(null, thisPriceByVolume);
		} else {
			callback(new Error("??????"));
		}
	});
}

var removePriceByVolume = function removePriceByVolume(wineMakerId, wineId, volumeId, callback) {
	PriceByVolume.findOneAndRemove({
		wineMakerId: wineMakerId,
		wineId: wineId,
		volumeId: volumeId
	}, function(err, result) {
		if (err) {
			return callback(err);
		}
		if (result) {
			callback(null, true);
		} else {
			callback(new Error("does not exist"));
		}
	});
}

module.exports = {
	getPriceByVolume: getPriceByVolume,
	getPriceByVolumeByWineId: getPriceByVolumeByWineId,
	addPriceByVolume: addPriceByVolume,
	editPriceByVolume: editPriceByVolume,
	removePriceByVolume: removePriceByVolume
}