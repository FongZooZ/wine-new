var WineMaker = require('./models/wineMaker');

var getOneWineMaker = function getOneWineMaker(wineMakerId, callback) {
	WineMaker.findOne({
		_id: wineMakerId
	}, function(err, thisWineMaker) {
		if (err) {
			return callback(err);
		}
		if (thisWineMaker) {
			callback(null, thisWineMaker);
		} else {
			callback(new Error("WineMaker with id " + wineMakerId + "does not exsit"));
		}
	});
}

var getAllWineMaker = function getAllWineMaker(callback) {
	WineMaker.find({}, callback);
}

var addWineMaker = function addWineMaker(data, callback) {
	WineMaker.findOne({
		wineMakerName: data.wineMakerName
	}, function function_name(err, thisWineMaker) {
		if (err) {
			return callback(err);
		}
		if (!thisWineMaker) {
			WineMaker.create({
				wineMakerName: data.wineMakerName,
				countryID: data.countryID
			}, function(err, wineMaker) {
				if (err) {
					return callback(err);
				}
				if (wineMaker) {
					callback(null, true);
				} else {
					callback(new Error("something error, can't create wine maker"));
				}
			});
		} else {
			callback(new Error("Exist wine type with winemaker name = " + data.wineMakerName));
		}
	});
}

var editWineMaker = function editWineMaker(wineMakerId, data, callback) {
	WineMaker.findOneAndUpdate({
		_id: wineMakerId
	}, {
		wineMakerName: data.wineMakerName,
		countryID: data.countryID
	}, {
		new: false,
		upsert: false,
		multi: false
	}, function(err, thisWineMaker) {
		if (err) {
			return callback(err);
		}
		if (thisWineMaker) {
			callback(null, thisWineMaker);
		} else {
			callback(new Error("??????"));
		}
	});
}

module.exports = {
	getOneWineMaker: getOneWineMaker,
	getAllWineMaker: getAllWineMaker,
	addWineMaker: addWineMaker,
	editWineMaker: editWineMaker
}