var Country = require('./models/country');
var async = require('async');
var ParseService = require('./ParseService');

var getOneCountry = function getOneCountry(countryId, callback) {
	Country.findOne({
		_id: countryId
	}, function(err, thisCountry) {
		if (err) {
			return callback(err);
		}
		console.log(thisCountry);
		if (thisCountry) {
			callback(null, thisCountry);
		} else {
			callback(new Error("Country with id " + countryId + " does not exsit"));
		}
	});
}

var getAllCountry = function getAllCountry(callback) {
	Country.find({}, callback);
}

var addCountry = function addCountry(data, callback) {
	Country.findOne({
		countryCode: data.countryCode
	}, function function_name(err, thisCountry) {
		if (err) {
			return callback(err);
		}
		if (!thisCountry) {
			Country.create({
				countryCode: data.countryCode,
				name: data.name,
				flagImg: data.flagImg
			}, function(err, country) {
				if (err) {
					return callback(err);
				}
				if (country) {
					callback(null, true);
				} else {
					callback(new Error("something error, can't create country"));
				}
			});
		} else {
			callback(new Error("Exist country with countryCode = " + data.countryCode));
		}
	});
}

var editCountry = function addCountry(countryId, data, callback) {
	Country.findOneAndUpdate({
		_id: countryId
	}, {
		countryCode: data.countryCode,
		name: data.countryCodedata.countryCode,
		flagImg: data.countryCode
	}, {
		new: false,
		upsert: false,
		multi: false
	}, function(err, thisCountry) {
		if (err) {
			return callback(err);
		}
		if (thisCountry) {
			callback(null, true);
		} else {
			callback(new Error("??????"));
		}
	});
}

var addCountriesFromXML = function(xmlPath, callback) {
	Country.remove({}, function(err) {
		if (err) {
			callback(err);
		}
		ParseService.toJS(xmlPath, function(err, countries) {
			if (err) {
				console.log(err);
			}
			var countriesDataTemp = countries.countries.country;
			var countryDataTemp = {};
			var countriesData = [];

			async.forEach(countriesDataTemp, function(country, cb) {
				var countryDataTemp = {
					countryCode: country.$.countryID,
					name: country.$.countryName,
					flagImg: country.$.flagImg
				}
				countriesData.push(countryDataTemp);
				countryDataTemp = {};
				cb();
			}, function(err) {
				if (err) {
					console.log(err);
				}
				async.forEach(countriesData, function(country, cb) {
					addCountry(country, function(result) {
						console.log(result);
					});
					cb();
				}, function(err) {
					if (err) {
						console.log(err)
					}
					callback();
				});
			});
		});
	});
}

module.exports = {
	getOneCountry: getOneCountry,
	getAllCountry: getAllCountry,
	addCountry: addCountry,
	editCountry: editCountry,
	addCountriesFromXML: addCountriesFromXML
}