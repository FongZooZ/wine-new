var Volume = require('./models/volume');

var getOneVolume = function getOneVolume(name, callback) {
	Volume.findOne({
		name: name
	}, function(err, thisVolume) {
		if (err) {
			return callback(err);
		}
		if (thisVolume) {
			callback(null, thisVolume);
		} else {
			callback(new Error("Volume with name " + name + "does not exsit"));
		}
	});
}

var getAllVolume = function getAllVolume(callback) {
	Volume.find({}, callback);
}

var addVolume = function addVolume(data, callback) {
	Volume.findOne({
		name: data.name
	}, function function_name(err, thisVolume) {
		if (err) {
			return callback(err);
		}
		if (!thisVolume) {
			Volume.create({
				volume: data.volume,
				name: data.name,
				note: data.note
			}, function(err, volume) {
				if (err) {
					return callback(err);
				}
				if (volume) {
					callback(null, true);
				} else {
					callback(new Error("something error, can't add this volume"));
				}
			});
		} else {
			callback(new Error("Exist country with volume = " + thisVolume));
		}
	});
}

var editVolume = function editVolume(name, data, callback) {

	Volume.findOneAndUpdate({
		_id: countryId
	}, {
		volume: data.volume,
		name: data.name,
		note: data.note
	}, {
		new: false,
		upsert: false,
		multi: false
	}, function(err, thisVolume) {
		if (err) {
			return callback(err);
		}
		if (thisVolume) {
			callback(null, thisVolume);
		} else {
			callback(new Error("??????"));
		}
	});
}

module.exports = {
	getOneVolume: getOneVolume,
	getAllVolume: getAllVolume,
	addVolume: addVolume,
	editVolume: editVolume
}