var WineType = require('./models/wineType');

var getOneWineType = function getOneWineType(wineTypeId, callback) {
	WineType.findOne({
		_id: wineTypeId
	}, function(err, thisWineType) {
		if (err) {
			return callback(err);
		}
		if (thisWineType) {
			callback(null, thisWineType);
		} else {
			callback(new Error("WineType with id " + wineTypeId + "does not exsit"));
		}
	});
}

var getAllWIneType = function getAllWIneType(callback) {
	WineType.find({}, callback);
}

var addWineType = function addWineType(data, callback) {
	WineType.findOne({
		typeName: data.typeName
	}, function function_name(err, thisWineType) {
		if (err) {
			return callback(err);
		}
		if (!thisWineType) {
			WineType.create({
				typeName: data.typeName,
				note: data.note
			}, function(err, wineType) {
				if (err) {
					return callback(err);
				}
				if (wineType) {
					callback(null, true);
				} else {
					callback(new Error("something error, can't create 'wine type'"));
				}
			});
		} else {
			callback(new Error("Exist wine type with typeName = " + data.typeName));
		}
	});
}

var editWineType = function editWineType(wineTypeId, data, callback) {
	WineType.findOneAndUpdate({
		_id: wineTypeId
	}, {
		typeName: data.typeName,
		note: data.note
	}, {
		new: false,
		upsert: false,
		multi: false
	}, function(err, thisWineType) {
		if (err) {
			return callback(err);
		}
		if (thisWineType) {
			callback(null, thisWineType);
		} else {
			callback(new Error("??????"));
		}
	});
}

module.exports = {
	getOneWineType: getOneWineType,
	getAllWIneType: getAllWIneType,
	addWineType: addWineType,
	editWineType: editWineType
}