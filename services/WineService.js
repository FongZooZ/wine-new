var Wine = require('./models/wine');

var getOneWine = function getOneWine(wineName, wineMakerId, callback) {
	Wine.findOne({
		wineMakerId: wineMakerId,
		name: wineName
	}, function(err, thisWine) {
		if (err) {
			return callback(err);
		}
		if (thisWine) {
			callback(null, thisWine);
		} else {
			callback(new Error("Wine with name " + wineName + "does not exsit"));
		}
	});
}

var getAllWine = function getAllWine(callback) {
	Wine.find({}, callback);
}

var addWine = function addWine(data, callback) {
	if (!data.name) {
		return callback(new Error('Name is not filled'));
	}
	if (!data.wineMakerId) {
		return callback(new Error('Wine maker is not found'));
	}
	Wine.findOne({
		name: data.name || "",
		wineMakerId: data.wineMakerId || ""
	}, function function_name(err, thisWine) {
		if (err) {
			return callback(err);
		}
		if (!thisWine) {
			Wine.create({
				name: data.name || "",
				imgSrc: null,
				color: data.color || "",
				rating: data.rating ,
				wineTypeId: data.wineTypeId || "",
				wineMakerId: data.wineMakerId || "",
				characteristic: {
					blend: data.characteristic.blend || "",
					strength: data.characteristic.strength || "",
					totalAcidity: data.characteristic.totalAcidity || "",
					volatileAcidity: data.characteristic.volatileAcidity || "",
					sulphurDioxide: data.characteristic.sulphurDioxide || "",
					copper: data.characteristic.copper || "",
					iron: data.characteristic.iron || "",
					sterility: data.characteristic.sterility || "",
					proteinStability: data.characteristic.proteinStability || "",
					pH: data.characteristic.pH || "",
					residualSugar: data.characteristic.residualSugar || ""
				},
				description: {
					brief: data.description.brief || "",
					overview: data.description.overview || "",
					flavorAndAroma: data.description.flavorAndAroma || "",
					agingPotential: data.description.agingPotential || "",
					throughForFood: data.description.throughForFood || "",
					mood: data.description.mood
				}
			}, function(err, wine) {
				if (err) {
					return callback(err);
				}
				if (wine) {
					callback(null, true);
				} else {
					callback(new Error("something error, can't create new wine '"));
				}
			});
		} else {
			callback(new Error("Exist wine with name = " + data.wineName));
		}
	});
}

var editWine = function editWine(wineName, wineMakerId, data, callback) {
	Wine.findOneAndUpdate({
		name: wineName,
		wineMakerId: wineMakerId
	}, {
		name: data.name,
		imgSrc: data.imgSrc || null,
		color: data.color,
		rating: data.rating,
		wineTypeId: data.wineTypeId,
		characteristic: {
			blend: data.blend,
			strength: data.strength,
			totalAcidity: data.totalAcidity,
			volatileAcidity: data.volatileAcidity,
			sulphurDioxide: data.sulphurDioxide,
			copper: data.copper,
			iron: data.iron,
			sterility: data.sterility,
			proteinStability: data.proteinStability,
			pH: data.pH,
			residualSugar: data.residualSugar
		},
		description: {
			brief: data.brief,
			overview: data.overview,
			flavorAndAroma: data.flavorAndAroma,
			agingPotential: data.agingPotential,
			throughForFood: data.throughForFood,
			mood: data.mood
		}
	}, {
		new: false,
		upsert: false,
		multi: false
	}, function(err, thisWine) {
		if (err) {
			return callback(err);
		}
		if (thisWine) {
			callback(null, thisWine);
		} else {
			callback(new Error("??????"));
		}
	});
}

module.exports = {
	getOneWine: getOneWine,
	getAllWine: getAllWine,
	addWine: addWine,
	editWine: editWine
}