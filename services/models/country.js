var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var countrySchema = new Schema({
	countryCode: String,
	name: String,
	flagImg: String
});

var Country = mongoose.model('country', countrySchema, 'country');

module.exports = Country;