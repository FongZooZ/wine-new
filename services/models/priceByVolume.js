var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var priceByVolumeSchema = new Schema({
  wineMakerId: ObjectId,
  wineId: ObjectId,
  volumeId: ObjectId,
  price: Number
});

var PriceByVolume = mongoose.model('priceByVolume', priceByVolumeSchema, "priceByVolume");

module.exports = PriceByVolume;