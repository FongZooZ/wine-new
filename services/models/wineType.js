var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var wineTypeSchema = new Schema({
	typeName: String,
	note: String
});

var WineType = mongoose.model('wineType', wineTypeSchema, 'wineType');

module.exports = WineType;