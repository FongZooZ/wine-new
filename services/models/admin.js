var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var adminSchema = new Schema({
	username: String,
	password: String
});

var Admin = mongoose.model('admin', adminSchema, 'admin');

module.exports = Admin;