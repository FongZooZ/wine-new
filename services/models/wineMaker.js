var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var wineMakerSchema = new Schema({
	wineMakerName: String,
	countryID: ObjectId
});

var WineMaker = mongoose.model('wineMaker', wineMakerSchema, 'wineMaker');

module.exports = WineMaker;