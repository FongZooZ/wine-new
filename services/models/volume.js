var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var volumeSchema = new Schema({
	volume: Number,
	name: String,
	note: String
});

var Volume = mongoose.model('volume', volumeSchema, 'volume');

module.exports = Volume;