var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var wineSchema = new Schema({
  name: String,
  imgSrc: String,
  color: String,
  rating: Number,
  wineTypeId: ObjectId,
  wineMakerId: ObjectId,
  characteristic: {
    blend: String,
    strength: Number,
    totalAcidity: Number,
    volatileAcidity: Number,
    sulphurDioxide: Number,
    copper: Number,
    iron: Number,
    sterility: Number,
    proteinStability: Number,
    pH: Number,
    residualSugar: Number
  },
  description: {
    brief: String,
    overview: String,
    flavorAndAroma: String,
    agingPotential: String,
    throughForFood: String,
    mood: String
  }
});

var Wine = mongoose.model('wine', wineSchema, "wine");

module.exports = Wine;