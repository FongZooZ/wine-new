var updateCountryList = function updateCountryList(cb) {
	$.ajax({
		url: '/countries',
		success: cb
	});
}

function CountryController($scope, $rootScope) {
	$scope.countries = countries;
	$scope.newCountry = {};

	var updateList = function() {
		updateCountryList(function(countries) {
			$scope.$apply(function() {
				$scope.countries = countries;
			});
		});
	}

	$scope.addCountry = function() {

	}

	$rootScope.$on('newCountry.added', updateList);
}