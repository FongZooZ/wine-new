var updateWineMakerList = function updateWineMakerList(cb) {
	$.ajax({
		url: '/wineMakers',
		success: cb
	});
}

var updateCountryList = function updateCountryList(cb) {
	$.ajax({
		url: '/countries',
		success: cb
	});
}

function WineMakerController($scope, $rootScope) {
	$scope.wineMakers = wineMakers;
	$scope.newWineMaker = {};

	$scope.countries = countries;

	var updateList = function() {
		updateWineMakerList(function(wineMakers) {
			$scope.$apply(function() {
				$scope.wineMakers = wineMakers;
			});
		});
	}

	$scope.addWineMaker = function() {
		$.ajax({
			url: '/admin/addWineMaker',
			method: 'post',
			data: $scope.newWineMaker,
			success: function(result) {
				$rootScope.$emit('newWineMaker.changed');
				$scope.newWineMaker = {};
				$("#add-new-wineMaker").modal("hide");
			}
		});
	}

	$scope.getCountryById = function(countryID) {
		for (var i = 0; i < countries.length; i++) {
			if (countries[i]._id == countryID) {
				var countryList = [];
				countryList.push(countries[i]);
				return countryList;
			}
		}
	}

	$rootScope.$on('newWineMaker.changed', updateList);
}