var updateWineTypeList = function updateWineTypeList(cb) {
	$.ajax({
		url: '/wineTypes',
		success: cb
	});
}

function WineTypeController($scope, $rootScope) {
	$scope.wineTypes = wineTypes;
	$scope.newWineTypes = {};

	var updateList = function() {
		updateWineTypeList(function(wineTypes) {
			$scope.$apply(function() {
				$scope.wineTypes = wineTypes;
			});
		});
	}

	$scope.addWineType = function() {
		$.ajax({
			url: '/admin/addWineType',
			method: 'post',
			data: $scope.newWineTypes,
			success: function(result) {
				$rootScope.$emit('newWineType.changed');
				$scope.newWineTypes = {};
				$("#add-new-type").modal("hide");
			}
		});
	}

	$rootScope.$on('newWineType.changed', updateList);
}