function WineController($scope, $rootScope) {
	$scope.wines = wines;
	$scope.newWine = {};

	$scope.wineMakers = wineMakers;
	$scope.wineTypes = wineTypes;

	$scope.ratings = [{
		label: "1",
		value: 1
	}, {
		label: "2",
		value: 2
	}, {
		label: "3",
		value: 3
	}, {
		label: "4",
		value: 4
	}, {
		label: "5",
		value: 5
	}, {
		label: "6",
		value: 6
	}, {
		label: "7",
		value: 7
	}, {
		label: "8",
		value: 8
	}, {
		label: "9",
		value: 9
	}, {
		label: "10",
		value: 10
	}, ];

	var updateList = function() {
		updateWineList(function(wines) {
			$scope.$apply(function() {
				$scope.wines = wines;
			});
		});
		updateWineTypeList(function(wineTypes) {
			$scope.$apply(function() {
				$scope.wineTypes = wineTypes;
			});
		});
		updateWineMakerList(function(wineMakers) {
			$scope.$apply(function() {
				$scope.wineMakers = wineMakers;
			});
		});
	}

	$scope.addWine = function() {
		$.ajax({
			url: '/admin/addWine',
			method: 'post',
			data: $scope.newWine,
			success: function(result) {
				$rootScope.$emit('newWine.changed');
				$scope.newWine = {};
				$("#add-new-wine").modal("hide");
			}
		});
	}

	$scope.getWineTypeById = function(wineTypeId) {
		for (var i = 0; i < wineTypes.length; i++) {
			if (wineTypes[i]._id == wineTypeId) {
				var wineTypeList = [];
				wineTypeList.push(wineTypes[i]);
				return wineTypeList;
			}
		}
	}

	$scope.getWineMakerById = function(wineMakerId) {
		for (var i = 0; i < wineMakers.length; i++) {
			if (wineMakers[i]._id == wineMakerId) {
				var wineMakerList = [];
				wineMakerList.push(wineMakers[i]);
				return wineMakerList;
			}
		}
	}

	$rootScope.$on('newWine.changed', updateList);
	$rootScope.$on('newWineMaker.changed', updateList);
	$rootScope.$on('newWineType.changed', updateList);
}

var updateWineList = function updateWineList(cb) {
	$.ajax({
		url: '/wines',
		success: cb
	});
}

var updateWineTypeList = function updateWineTypeList(cb) {
	$.ajax({
		url: '/wineTypes',
		success: cb
	});
}

var updateWineMakerList = function updateWineMakerList(cb) {
	$.ajax({
		url: '/wineMakers',
		success: cb
	});
}