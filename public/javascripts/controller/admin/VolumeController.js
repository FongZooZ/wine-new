var updateVolumeList = function updateVolumeList(cb) {
	$.ajax({
		url: '/volumes',
		success: cb
	});
}

function VolumeController($scope, $rootScope) {
	$scope.volumes = volumes;
	$scope.newVolume = {};

	var updateList = function() {
		updateVolumeList(function(volumes) {
			$scope.$apply(function() {
				$scope.volumes = volumes;
			});
		});
	}

	$scope.addVolume = function() {
		$.ajax({
			url: '/admin/addVolume',
			method: 'post',
			data: $scope.newVolume,
			success: function(result) {
				$rootScope.$emit('newVolume.changed');
				$scope.newVolume = {};
				$("#add-new-volume").modal("hide");
			}
		});
	}

	$rootScope.$on('newVolume.changed', updateList);
}