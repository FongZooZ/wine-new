function LoginController($scope, $rootScope) {
  $scope.admin = {};

  /**
   * function is not in use
   */
  // $scope.login = function() {
  //   $.ajax({
  //     url: '/login',
  //     method: 'post',
  //     data: $scope.user,
  //     success: function(result) {
  //       $rootScope.$emit("user.login");
  //       $scope.user = {};
  //     }
  //   });
  // }

  $scope.login = function() {
    $rootScope.$emit("admin.login");
  }
}